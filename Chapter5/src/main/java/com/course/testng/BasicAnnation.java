package com.course.testng;

import org.testng.annotations.*;

public class BasicAnnation {
    //最基本的注解，用来把方法标记为测试的一部分
    @Test
    public void testCase1(){
        System.out.println("这是测试用例1");
    }
    @Test
    public void testCase2(){
        System.out.println("这是测试用例2");
    }
    @BeforeMethod
    public void beforeMethod() {
        System.out.println("---方法开始执行---");
    }
    @AfterMethod
    public void afterMethod() {
        System.out.println("---方法结束执行---");
    }
    @BeforeClass
    public void beforeClass() {
        System.out.println("---类开始执行---");
    }
    @AfterClass
    public void afterClass() {
        System.out.println("---类结束执行---");
    }
    //测试套件内可以包含多个类
    @BeforeSuite
    public void beforeSuit() {
        System.out.println("---测试套件开始执行---");
    }
    @AfterSuite
    public void afterSuit() {
        System.out.println("---测试套件结束执行---");
    }
}


