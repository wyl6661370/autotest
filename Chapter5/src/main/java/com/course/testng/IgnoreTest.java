package com.course.testng;

import org.testng.annotations.Test;

public class IgnoreTest {
    //忽略测试，可以看出test默认是执行的，默认enabled = true
    @Test
    public void ignore1(){
        System.out.println("ignore1执行！");
    }
    @Test(enabled = true)
    public void ignore2(){
        System.out.print("ignore2执行!");
    }
    @Test(enabled = false)
    public void ignore3(){
        System.out.print("ignore3执行!");
    }

}
